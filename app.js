const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


//Habilitar servidor..
app.listen(port, () => console.log(`Servidor rodando...${port}`));

//recurso de request.aluno..
app.get('/aluno',(req,res)=>{
  res.send('Vai Trikas, hoje voces nao ganham....kkkk');
});

//recurso de request.filtros..
app.get('/aluno/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados solicitado: "+ source.nome + " " + source.sobrenome;
    res.send("{message: "+ret+" }");
  });

  //recurso de request.pesquisa..
app.get('/aluno/pesquisa/:valor',(req,res)=>{
    console.log('entrou');
    let dado = req.params.valor;
    let ret = "Dados solicitado: "+ dado;
    res.send("{message: "+ret+" }");
  });

  //recurso de request.body
  app.post('/aluno', (req,res)=>{
    let dados = req.body;
    let ret = "Dados enviados: "+ dados.nome;
    ret+=" Sobrenome: "+dados.sobrenome;
    ret+=" idade: "+dados.idade;
    res.json(dados);
  });



